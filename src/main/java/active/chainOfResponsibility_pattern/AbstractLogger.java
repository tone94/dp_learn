package active.chainOfResponsibility_pattern;

public abstract class AbstractLogger {
    protected static int ERROR = 3;
    protected static int FILE = 2;
    protected static int CONSOLE = 1;

    protected int level;
    //责任链中的下一个元素
    protected AbstractLogger nextLogger;

    public void setNextLogger(AbstractLogger nextLogger){
        this.nextLogger = nextLogger;
    }
    public void logMessage(int level, String msg){
        if (this.level<=level){
            write(msg);
        }
        if (nextLogger !=null){
            nextLogger.logMessage(level,msg);
        }
    }

    public abstract void write(String msg);

}
