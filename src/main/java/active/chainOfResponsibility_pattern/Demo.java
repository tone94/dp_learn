package active.chainOfResponsibility_pattern;

public class Demo {

    private static AbstractLogger getChain(){
        AbstractLogger error = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger file = new FileLogger(AbstractLogger.FILE);
        AbstractLogger console = new ConsoleLogger(AbstractLogger.CONSOLE);

        error.setNextLogger(file);
        file.setNextLogger(console);

        return error;
    }

    public static void main(String[] args) {
        AbstractLogger logger = getChain();
        logger.logMessage(AbstractLogger.CONSOLE,"msg1");
        logger.logMessage(AbstractLogger.FILE,"msg2");
        logger.logMessage(AbstractLogger.ERROR,"msg3");
    }

}
