package create.builder_pattern.KFCExample;

/**
 * 表示食物类目
 */
public interface Item {
     String name();
     Packing packing();
     float price();
}
