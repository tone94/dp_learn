package create.builder_pattern.KFCExample;

public class BuiderPatternDemo {
    public static void main(String[] args) {
        MealBuilder mealBuilder = new MealBuilder();

        Meal vegMeal = mealBuilder.prepareVegMeal();
        System.out.println("veg meal");
        vegMeal.showItems();
        System.out.println("total cost :"+vegMeal.getCost());

        Meal meal = mealBuilder.prepareNonVegMeal();
        System.out.println("---------------\nnon veg meal");
        meal.showItems();
        System.out.println("total cost:"+ meal.getCost());
    }
}
