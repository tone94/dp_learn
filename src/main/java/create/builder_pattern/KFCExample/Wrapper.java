package create.builder_pattern.KFCExample;

public class Wrapper implements Packing {

    @Override
    public String pack() {
        return "Wrapper";
    }
}
