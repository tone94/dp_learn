package create.builder_pattern.KFCExample;

public class Bottle implements Packing {
    @Override
    public String pack() {
        return "Bottle";
    }
}
