package create.builder_pattern.KFCExample;

/**
 * 包装类
 */
public interface Packing {
    String pack();
}
