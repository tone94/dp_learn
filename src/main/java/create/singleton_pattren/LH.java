package create.singleton_pattren;

/**
 * 线程不安全的懒汉式
 * 这种方式是最基本的实现方式，这种实现最大的问题就是不支持多线程。
 * 因为没有加锁 synchronized，所以严格意义上它并不算单例模式
 */
public class LH {
    private static LH instance;

    private LH (){}

    public static LH getInstance(){
        if(instance == null){
            instance = new LH();
        }
        return instance;
    }
}

/**
 * 线程安全的懒汉式
 * 这种方式具备很好的 lazy loading，能够在多线程中很好的工作，但是，效率很低，99% 情况下不需要同步。
 */
class LH2{
    private static LH2 instance;

    private LH2 (){}

    public static synchronized LH2 getInstance(){
        if(instance == null){
            instance = new LH2();
        }
        return instance;
    }

}