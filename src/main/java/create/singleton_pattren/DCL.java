package create.singleton_pattren;

/**
 * 双检锁/双重校验锁 (double-checked locking)
 *
 * JDK 版本：JDK1.5 起
 * 这种方式采用双锁机制，安全且在多线程情况下能保持高性能
 */
public class DCL {
    // volatile 关键字
    private volatile static DCL instance;
    private DCL (){}

    /**
     * 为了避免每次判断instance是否为null都加锁, 于是加入双重判断机制, 提高了效率
     * @return
     */
    public static DCL getInstance(){
        if(instance == null){
            synchronized (DCL.class){
                if (instance == null){
                    instance = new DCL();
                }
            }
        }
        return instance;
    }
}
