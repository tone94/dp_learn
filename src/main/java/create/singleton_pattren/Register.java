package create.singleton_pattren;

/**
 * 登记式/静态内部类
 *
 * 这种方式只适用于静态域的情况，双检锁方式可在实例域需要延迟初始化时使用
 * 它跟饿汉式不同的是：饿汉式只要 Singleton 类被装载了，那么 instance 就会被实例化（没有达到 lazy loading 效果），
 * 而这种方式是 Singleton 类被装载了，instance 不一定被初始化。
 * 因为 SingletonHolder 类没有被主动使用，只有通过显式调用 getInstance 方法时，才会显式装载 SingletonHolder 类，从而实例化 instance
 *
 * 只有在要明确实现 lazy loading 效果时, 才使用这种方式
 */
public class Register {

    private static class SingletonHolder{
        private static final Register INSTANCE = new Register();
    }

    private Register() {}
    public static final Register getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
