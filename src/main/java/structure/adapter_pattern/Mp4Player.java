package structure.adapter_pattern;

public class Mp4Player implements AdvanceMediaPlayer{

    @Override
    public void playVlc(String fileName) {
        // nothing
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("playing mp4 file. name: "+fileName);
    }
}
