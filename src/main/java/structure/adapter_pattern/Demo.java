package structure.adapter_pattern;

public class Demo {

    public static void main(String[] args) {
        AudioPlayer ap = new AudioPlayer();
        ap.play("mp3","hello ");
        ap.play("mp4","hello ");
        ap.play("vlc","hello ");
        ap.play("mp5","hello ");
    }
}
