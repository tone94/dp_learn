package structure.adapter_pattern;

public class AudioPlayer implements MediaPlayer{
    @Override
    public void play(String audioType, String fileName) {
        if(audioType.equalsIgnoreCase("mp3")){
            System.out.println("playing mp3 file. name: "+fileName);
        }
        else if(audioType.equalsIgnoreCase("mp4")||audioType.equalsIgnoreCase("vlc")){
            MediaAdapter media = new MediaAdapter(audioType);
            media.play(audioType, fileName);
        }
        else{
            System.out.println("no that type");
        }
    }
}
